# Package API Laravel App

Aplikasi ini dibuat untuk memenuhi online test Backend Engineer Mileapp

## Instalasi

Setelah melakukan clone, copy `.env.example` kemudian rename menjadi `.env` kemudian jalankan :

```bash
  composer install
  php artisan serve
```

Kemudian running server pada **http://localhost:8000**

## Database

Database menggunakan MongoDB dan diset menggunakan Cloud MongoDB Atlas. Apabila database ingin dijalankan di local menggunakan MongoDB Compass bisa edit `database/config.app` kemudian sesuaikan `.env`

```php
'connections' => [
        ......
        'mongodb' => [
            'driver'   => 'mongodb',
            'host'     => env('MONGO_DB_HOST', 'localhost'),
            'port'     => env('MONGO_DB_PORT', 27017),
            'database' => env('MONGO_DB_DATABASE'),
            'username' => env('MONGO_DB_USERNAME'),
            'password' => env('MONGO_DB_PASSWORD'),
            'options'  => []
        ],
    ]
```

## Documentation

[API Documentation](https://app.swaggerhub.com/apis-docs/noaditya10/PackageRESTfulAPI/1.0)

## Perbedaan PUT dan PATCH

#### PUT

PUT digunakan untuk mengubah data dengan cara mereplace data sepenuhnya.
Contoh :

| Data dalam DB          | Request                        | Setelah update PUT     |
| :--------------------- | :----------------------------- | :--------------------- |
| username : Novi        | username : Novi Aditya         | username : Novi Aditya |
| email : novi@gmail.com | _tidak ada request email/NULL_ | email : NULL           |

#### PATCH

Sementara PATCH dapat digunakan untuk mengubah HANYA pada data yang ingin dirubah sesuai request.
Contoh :

| Data dalam DB          | Request                        | Setelah update PATCH   |
| :--------------------- | :----------------------------- | :--------------------- |
| username : Novi        | username : Novi Aditya         | username : Novi Aditya |
| email : novi@gmail.com | _tidak ada request email/NULL_ | email : novi@gmail.com |
