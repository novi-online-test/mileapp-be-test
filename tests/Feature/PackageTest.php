<?php

namespace Tests\Feature;

use App\Models\Package;
use Carbon\Carbon;
use Faker\Generator;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Support\Str;
use Tests\TestCase;

class PackageTest extends TestCase
{

    public function test_index()
    {
        $response = $this->get(route('package.index'));

        $response->assertStatus(200);
    }

    /**
     * @var $id get random _id
     */
    public function test_show_success()
    {
        $id = Package::get()->random(1)->pluck('_id')->first();

        $response = $this->get(route('package.show', $id));

        $response->assertStatus(200);
    }

    public function test_show_fail()
    {
        $id = Str::random(10);

        $response = $this->get(route('package.show', $id));

        $response->assertStatus(404);
    }

    public function test_store()
    {
        $response = $this->postJson(route('package.store'), [
            'transaction_id'                        => 'd0326c40-890f-j532-0965-899b9970cfew',
            'customer_name'                         => 'PT. EKSPEDISI SATU',
            'customer_code'                         => 1674324,
            'transaction_amount'                    => 120000,
            'transaction_discount'                  => 0,
            'transaction_additional_field'          => "",
            'transaction_payment_type'              => 29,
            'transaction_payment_state'             => "PAID",
            'transaction_code'                      => "JGJSL20200715121",
            'transaction_order'                     => 132,
            'location_id'                           => Str::random(24),
            'organization_id'                       => rand(1, 1000),
            'created_at'                            => Carbon::now(),
            'updated_at'                            => Carbon::now(),
            'transaction_payment_type_name'         => "Invoice",
            'transaction_cash_amount'               => 0,
            'transaction_cash_change'               => 0,
            'customer_attribute' => [
                'Nama_Sales'         => app(Generator::class)->name,
                'TOP'                => "15 Hari",
                'Jenis_Pelanggan'    => "B2B",
            ],

            'connote' => [
                'connote_id'                    => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
                'connote_number'                => rand(1, 100),
                'connote_service'               => "ECO",
                'connote_service_price'         => rand(10000, 100000),
                'connote_amount'                => rand(10000, 100000),
                'connote_code'                  => Str::random(17),
                'connote_booking_code'          => "",
                'connote_order'                 => rand(100000, 999999),
                'connote_state'                 => "PAID",
                'connote_state_id'              => rand(1, 10),
                'zone_code_from'                => Str::random(5),
                'zone_code_to'                  => Str::random(5),
                'surcharge_amount'              => rand(0, 100000),
                'transaction_id'                => "d0090c40-539f-479a-8274-899b9970bddc",
                'actual_weight'                 => rand(0, 1000),
                'volume_weight'                 => rand(0, 1000),
                'chargeable_weight'             => rand(0, 1000),
                'created_at'                    => Carbon::now(),
                'updated_at'                    => Carbon::now(),
                'organization_id'               => rand(1, 100),
                'location_id'                   => Str::random(24),
                'connote_total_package'         => rand(1, 100),
                'connote_surcharge_amount'      => 0,
                'connote_sla_day'               => rand(1, 30),
                'location_name'                 => "Hub Jogja",
                'location_type'                 => "HUB",
                'source_tariff_db'              => "tariff_customers",
                'id_source_tariff'              =>  rand(1000000, 9000000),
                'pod'                           => null,
                'history'                       => [],
            ],

            'connote_id'                            => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",

            'origin_data' => [
                'customer_name'             => app(Generator::class)->name,
                'customer_address'          => app(Generator::class)->address,
                'customer_email'            => app(Generator::class)->email,
                'customer_phone'            => app(Generator::class)->e164PhoneNumber,
                'customer_address_detail'   => null,
                'customer_zip_code'         => app(Generator::class)->postcode,
                'zone_code'                 => Str::random(5),
                'organization_id'           => rand(1, 100),
                'location_id'               => Str::random(24),
            ],

            'destination_data' => [
                'customer_name'             => app(Generator::class)->name,
                'customer_address'          => app(Generator::class)->address,
                'customer_email'            => app(Generator::class)->email,
                'customer_phone'            => app(Generator::class)->e164PhoneNumber,
                'customer_address_detail'   => null,
                'customer_zip_code'         => app(Generator::class)->postcode,
                'zone_code'                 => Str::random(5),
                'organization_id'           => rand(1, 100),
                'location_id'               => Str::random(24),
            ],

            'koli_data' => [
                [
                    'koli_length'               => rand(0, 100),
                    'awb_url'                   => app(Generator::class)->url,
                    'created_at'                => Carbon::now(),
                    'koli_chargeable_weight'    => rand(0, 100),
                    'koli_width'                => rand(0, 100),
                    'koli_surcharge'            => [],
                    'koli_height'               => rand(0, 100),
                    'updated_at'                => Carbon::now(),
                    'koli_description'          => null,
                    'koli_formula_id'           => null,
                    'connote_id'                => Str::random(36),
                    'koli_volume'               => rand(0, 100),
                    'koli_weight'               => rand(0, 100),
                    'koli_id'                   => Str::random(36),
                    'koli_code'                 => Str::random(25),
                ]
            ],

            'currentLocation' => [
                'name'               => "Hub Jogja",
                'code'               => Str::random(5),
                'type'               => "Agent",
            ],

        ]);

        $response->assertStatus(201);
    }

    public function test_put_update()
    {
        $id = Package::get()->random(1)->pluck('_id')->first();
        $response = $this->putJson(route('package.update', $id), [
            'transaction_id'                        => 'd0326c40-890f-j532-0965-899b9970cfew',
            'customer_name'                         => 'PT. ' . strtoupper(app(Generator::class)->name),
            'customer_code'                         => 1674324,
            'transaction_amount'                    => 120000,
            'transaction_discount'                  => 0,
            'transaction_additional_field'          => "",
            'transaction_payment_type'              => 29,
            'transaction_payment_state'             => "PAID",
            'transaction_code'                      => "JGJSL20200715121",
            'transaction_order'                     => 132,
            'location_id'                           => Str::random(24),
            'organization_id'                       => rand(1, 1000),
            'created_at'                            => Carbon::now(),
            'updated_at'                            => Carbon::now(),
            'transaction_payment_type_name'         => "Invoice",
            'transaction_cash_amount'               => 0,
            'transaction_cash_change'               => 0,
            'customer_attribute' => [
                'Nama_Sales'         => app(Generator::class)->name,
                'TOP'                => "15 Hari",
                'Jenis_Pelanggan'    => "B2B",
            ],

            'connote' => [
                'connote_id'                    => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
                'connote_number'                => rand(1, 100),
                'connote_service'               => "ECO",
                'connote_service_price'         => rand(10000, 100000),
                'connote_amount'                => rand(10000, 100000),
                'connote_code'                  => Str::random(17),
                'connote_booking_code'          => "",
                'connote_order'                 => rand(100000, 999999),
                'connote_state'                 => "PAID",
                'connote_state_id'              => rand(1, 10),
                'zone_code_from'                => Str::random(5),
                'zone_code_to'                  => Str::random(5),
                'surcharge_amount'              => rand(0, 100000),
                'transaction_id'                => "d0090c40-539f-479a-8274-899b9970bddc",
                'actual_weight'                 => rand(0, 1000),
                'volume_weight'                 => rand(0, 1000),
                'chargeable_weight'             => rand(0, 1000),
                'created_at'                    => Carbon::now(),
                'updated_at'                    => Carbon::now(),
                'organization_id'               => rand(1, 100),
                'location_id'                   => Str::random(24),
                'connote_total_package'         => rand(1, 100),
                'connote_surcharge_amount'      => 0,
                'connote_sla_day'               => rand(1, 30),
                'location_name'                 => "Hub Jogja",
                'location_type'                 => "HUB",
                'source_tariff_db'              => "tariff_customers",
                'id_source_tariff'              =>  rand(1000000, 9000000),
                'pod'                           => null,
                'history'                       => [],
            ],

            'connote_id'                            => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",

            'origin_data' => [
                'customer_name'             => app(Generator::class)->name,
                'customer_address'          => app(Generator::class)->address,
                'customer_email'            => app(Generator::class)->email,
                'customer_phone'            => app(Generator::class)->e164PhoneNumber,
                'customer_address_detail'   => null,
                'customer_zip_code'         => app(Generator::class)->postcode,
                'zone_code'                 => Str::random(5),
                'organization_id'           => rand(1, 100),
                'location_id'               => Str::random(24),
            ],

            'destination_data' => [
                'customer_name'             => app(Generator::class)->name,
                'customer_address'          => app(Generator::class)->address,
                'customer_email'            => app(Generator::class)->email,
                'customer_phone'            => app(Generator::class)->e164PhoneNumber,
                'customer_address_detail'   => null,
                'customer_zip_code'         => app(Generator::class)->postcode,
                'zone_code'                 => Str::random(5),
                'organization_id'           => rand(1, 100),
                'location_id'               => Str::random(24),
            ],

            'koli_data' => [
                [
                    'koli_length'               => rand(0, 100),
                    'awb_url'                   => app(Generator::class)->url,
                    'created_at'                => Carbon::now(),
                    'koli_chargeable_weight'    => rand(0, 100),
                    'koli_width'                => rand(0, 100),
                    'koli_surcharge'            => [],
                    'koli_height'               => rand(0, 100),
                    'updated_at'                => Carbon::now(),
                    'koli_description'          => null,
                    'koli_formula_id'           => null,
                    'connote_id'                => Str::random(36),
                    'koli_volume'               => rand(0, 100),
                    'koli_weight'               => rand(0, 100),
                    'koli_id'                   => Str::random(36),
                    'koli_code'                 => Str::random(25),
                ]
            ],

            'currentLocation' => [
                'name'               => "Hub Jogja",
                'code'               => Str::random(5),
                'type'               => "Agent",
            ],

        ]);

        $response->assertStatus(200);
    }

    public function test_patch_update()
    {
        $id = Package::get()->random(1)->pluck('_id')->first();
        $response = $this->patchJson(route('package.update', $id), [
            'transaction_id'                        => 'd0326c40-890f-j532-0965-899b9970cfew',
            'customer_name'                         => 'PT. ' . strtoupper(app(Generator::class)->name),
            'customer_code'                         => 1674324,
            'transaction_amount'                    => 120000,
            'transaction_discount'                  => 0,
            'transaction_additional_field'          => "",
            'transaction_payment_type'              => 29,
            'transaction_payment_state'             => "PAID",
            'transaction_code'                      => "JGJSL20200715121",
            'transaction_order'                     => 132,
            'location_id'                           => Str::random(24),
            'organization_id'                       => rand(1, 1000),
            'created_at'                            => Carbon::now(),
            'updated_at'                            => Carbon::now(),
            'transaction_payment_type_name'         => "Invoice",
            'transaction_cash_amount'               => 0,
            'transaction_cash_change'               => 0,
            'customer_attribute' => [
                'Nama_Sales'         => app(Generator::class)->name,
                'TOP'                => "15 Hari",
                'Jenis_Pelanggan'    => "B2B",
            ],

            'connote' => [
                'connote_id'                    => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",
                'connote_number'                => rand(1, 100),
                'connote_service'               => "ECO",
                'connote_service_price'         => rand(10000, 100000),
                'connote_amount'                => rand(10000, 100000),
                'connote_code'                  => Str::random(17),
                'connote_booking_code'          => "",
                'connote_order'                 => rand(100000, 999999),
                'connote_state'                 => "PAID",
                'connote_state_id'              => rand(1, 10),
                'zone_code_from'                => Str::random(5),
                'zone_code_to'                  => Str::random(5),
                'surcharge_amount'              => rand(0, 100000),
                'transaction_id'                => "d0090c40-539f-479a-8274-899b9970bddc",
                'actual_weight'                 => rand(0, 1000),
                'volume_weight'                 => rand(0, 1000),
                'chargeable_weight'             => rand(0, 1000),
                'created_at'                    => Carbon::now(),
                'updated_at'                    => Carbon::now(),
                'organization_id'               => rand(1, 100),
                'location_id'                   => Str::random(24),
                'connote_total_package'         => rand(1, 100),
                'connote_surcharge_amount'      => 0,
                'connote_sla_day'               => rand(1, 30),
                'location_name'                 => "Hub Jogja",
                'location_type'                 => "HUB",
                'source_tariff_db'              => "tariff_customers",
                'id_source_tariff'              =>  rand(1000000, 9000000),
                'pod'                           => null,
                'history'                       => [],
            ],

            'connote_id'                            => "f70670b1-c3ef-4caf-bc4f-eefa702092ed",

            'origin_data' => [
                'customer_name'             => app(Generator::class)->name,
                'customer_address'          => app(Generator::class)->address,
                'customer_email'            => app(Generator::class)->email,
                'customer_phone'            => app(Generator::class)->e164PhoneNumber,
                'customer_address_detail'   => null,
                'customer_zip_code'         => app(Generator::class)->postcode,
                'zone_code'                 => Str::random(5),
                'organization_id'           => rand(1, 100),
                'location_id'               => Str::random(24),
            ],

            'destination_data' => [
                'customer_name'             => app(Generator::class)->name,
                'customer_address'          => app(Generator::class)->address,
                'customer_email'            => app(Generator::class)->email,
                'customer_phone'            => app(Generator::class)->e164PhoneNumber,
                'customer_address_detail'   => null,
                'customer_zip_code'         => app(Generator::class)->postcode,
                'zone_code'                 => Str::random(5),
                'organization_id'           => rand(1, 100),
                'location_id'               => Str::random(24),
            ],

            'koli_data' => [
                [
                    'koli_length'               => rand(0, 100),
                    'awb_url'                   => app(Generator::class)->url,
                    'created_at'                => Carbon::now(),
                    'koli_chargeable_weight'    => rand(0, 100),
                    'koli_width'                => rand(0, 100),
                    'koli_surcharge'            => [],
                    'koli_height'               => rand(0, 100),
                    'updated_at'                => Carbon::now(),
                    'koli_description'          => null,
                    'koli_formula_id'           => null,
                    'connote_id'                => Str::random(36),
                    'koli_volume'               => rand(0, 100),
                    'koli_weight'               => rand(0, 100),
                    'koli_id'                   => Str::random(36),
                    'koli_code'                 => Str::random(25),
                ]
            ],

            'currentLocation' => [
                'name'               => "Hub Jogja",
                'code'               => Str::random(5),
                'type'               => "Agent",
            ],

        ]);

        $response->assertStatus(200);
    }

    public function test_destroy()
    {
        $id = Package::get()->random(1)->pluck('_id')->first();

        $response = $this->get(route('package.destroy', $id));

        $response->assertStatus(200);
    }
}
