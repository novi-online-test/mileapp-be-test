<?php

namespace App\Http\Controllers;

use App\Http\Requests\PackageRequest;
use App\Http\Resources\PackageResource;
use App\Models\Package;
use Illuminate\Http\JsonResponse;

class PackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return App\Http\Resources\PackageResource
     */
    public function index()
    {
        $packages = Package::get();

        return new PackageResource($packages, 200, "OK");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  App\Http\Requests\PackageRequest  $request
     * @return App\Http\Resources\PackageResource
     */
    public function store(PackageRequest $request)
    {
        $newPackage = Package::create($request->all());

        return new PackageResource($newPackage, 201, "CREATED");
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return App\Http\Resources\PackageResource
     */
    public function show($id)
    {
        $package = Package::findOrFail($id);

        return new PackageResource($package, 200, "OK");
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  App\Http\Requests\PackageRequest  $request
     * @param  Package  $package
     * @return App\Http\Resources\PackageResource
     */
    public function update(PackageRequest $request, Package $package)
    {
        $package->update($request->all());

        return new PackageResource($package, 200, "UPDATED");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  Package  $package
     * @return Illuminate\Http\JsonResponse;
     */
    public function destroy(Package $package)
    {
        $package->delete();

        return response()->json(["code" => 200, "status" => "DELETED"], 200);
    }
}
