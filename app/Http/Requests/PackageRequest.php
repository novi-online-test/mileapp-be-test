<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PackageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'transaction_id'                        => 'required|string|max:40',
            'customer_name'                         => 'required|max:255',
            'customer_code'                         => 'required|numeric|digits:7',
            'transaction_amount'                    => 'required|numeric',
            'transaction_discount'                  => 'required|numeric',
            'transaction_additional_field'          => 'nullable|string|max:50',
            'transaction_payment_type'              => 'required|numeric',
            'transaction_payment_state'             => 'required|string',
            'transaction_code'                      => 'required|string|max:50',
            'transaction_order'                     => 'required|numeric',
            'location_id'                           => 'required|string|max:24',
            'organization_id'                       => 'required|numeric',
            'created_at'                            => 'required|date',
            'updated_at'                            => 'required|date',
            'transaction_payment_type_name'         => 'required|string|max:50',
            'transaction_cash_amount'               => 'nullable|numeric',
            'transaction_cash_change'               => 'nullable|numeric',
            'customer_attribute.Nama_Sales'         => 'required|string|max:50',
            'customer_attribute.TOP'                => 'required|string|max:50',
            'customer_attribute.Jenis_Pelanggan'    => 'required|string|max:50',

            'connote.connote_id'                    => 'required|string|max:36',
            'connote.connote_number'                => 'required|numeric|digits_between:1,10',
            'connote.connote_service'               => 'required|string|max:50',
            'connote.connote_service_price'         => 'required|numeric|digits_between:1,10',
            'connote.connote_amount'                => 'required|numeric|digits_between:1,10',
            'connote.connote_code'                  => 'required|string|min:17|max:20',
            'connote.connote_booking_code'          => 'nullable|string|max:50',
            'connote.connote_order'                 => 'required|numeric|digits_between:1,10',
            'connote.connote_state'                 => 'required|string|max:50',
            'connote.connote_state_id'              => 'required|numeric|max:10',
            'connote.zone_code_from'                => 'required|string|max:10',
            'connote.zone_code_to'                  => 'required|string|max:10',
            'connote.surcharge_amount'              => 'nullable|numeric|digits_between:1,10',
            'connote.transaction_id'                => 'required|string|max:50',
            'connote.actual_weight'                 => 'nullable|numeric|digits_between:1,4',
            'connote.volume_weight'                 => 'nullable|numeric|digits_between:1,4',
            'connote.chargeable_weight'             => 'nullable|numeric|digits_between:1,4',
            'connote.created_at'                    => 'required|date',
            'connote.updated_at'                    => 'required|date',
            'connote.organization_id'               => 'required|numeric|digits_between:1,3',
            'connote.location_id'                   => 'required|string|max:24',
            'connote.connote_total_package'         => 'required|numeric|digits_between:1,3',
            'connote.connote_surcharge_amount'      => 'nullable|numeric|max:10',
            'connote.connote_sla_day'               => 'required|numeric|digits_between:1,3',
            'connote.location_name'                 => 'required|string|max:50',
            'connote.location_type'                 => 'required|string|max:10',
            'connote.source_tariff_db'              => 'required|string|max:50',
            'connote.id_source_tariff'              => 'required|numeric|digits_between:1,10',
            'connote.pod'                           => 'nullable|string|max:50',
            'connote.history'                       => 'nullable|array',

            'connote_id'                    => 'required|string|max:36',

            'origin_data.customer_name'             => 'required|string|max:50',
            'origin_data.customer_address'          => 'required|string|max:255',
            'origin_data.customer_email'            => 'nullable|email',
            'origin_data.customer_phone'            => 'required|numeric',
            'origin_data.customer_address_detail'   => 'nullable|string|max:255',
            'origin_data.customer_zip_code'         => 'required|string|max:12',
            'origin_data.zone_code'                 => 'required|string|max:12',
            'origin_data.organization_id'           => 'required|numeric|digits_between:1,3',
            'origin_data.location_id'               => 'required|string|max:24',

            'destination_data.customer_name'             => 'required|string|max:50',
            'destination_data.customer_address'          => 'required|string|max:255',
            'destination_data.customer_email'            => 'nullable|email',
            'destination_data.customer_phone'            => 'required|numeric',
            'destination_data.customer_address_detail'   => 'nullable|string|max:255',
            'destination_data.customer_zip_code'         => 'required|string|max:12',
            'destination_data.zone_code'                 => 'required|string|max:12',
            'destination_data.organization_id'           => 'required|numeric|digits_between:1,3',
            'destination_data.location_id'               => 'required|string|max:24',

            'koli_data.*.koli_length'               => 'required|numeric|digits_between:1,3',
            'koli_data.*.awb_url'                   => 'required|url|max:255',
            'koli_data.*.created_at'                => 'required|date',
            'koli_data.*.koli_chargeable_weight'    => 'required|numeric|digits_between:1,3',
            'koli_data.*.koli_width'                => 'nullable|numeric|digits_between:1,3',
            'koli_data.*.koli_surcharge'            => 'nullable|array',
            'koli_data.*.koli_height'               => 'nullable|numeric|digits_between:1,3',
            'koli_data.*.updated_at'                => 'required|date',
            'koli_data.*.koli_description'          => 'nullable|string|max:255',
            'koli_data.*.koli_formula_id'           => 'nullable|numeric|digits_between:1,3',
            'koli_data.*.connote_id'                => 'required|string|max:36',
            'koli_data.*.koli_volume'               => 'nullable|numeric|digits_between:1,3',
            'koli_data.*.koli_weight'               => 'nullable|numeric|digits_between:1,3',
            'koli_data.*.koli_id'                   => 'required|string|max:36',
            'koli_data.*.koli_code'                 => 'required|string|max:25',

            'currentLocation.name'               => 'required|string|max:50',
            'currentLocation.code'               => 'required|string|max:10',
            'currentLocation.type'               => 'required|string|max:25',
        ];
    }
}
