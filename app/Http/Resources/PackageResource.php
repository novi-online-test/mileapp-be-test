<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PackageResource extends JsonResource
{
    public function __construct($data, $statusCode, $status)
    {
        $this->data = $data;
        $this->statusCode = $statusCode;
        $this->status = $status;
    }

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return  [
            'code'  => $this->statusCode,
            'status' => $this->status,
            'data'  => $this->data,
        ];
    }
}
